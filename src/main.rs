// Calculate the occurrences in a given string.

use std::io;
use std::collections::HashMap;

fn main() {
	println!("Please input a string to elaborate.");

	let mut input = String::new();
	io::stdin().read_line(&mut input)
		.expect("Failed to read line");

	let mut scores: HashMap<char, i32> = HashMap::new();
	let mut chars = input[0..input.len() - 1].chars();

	while let Some(key) = chars.next() {
		let entry = scores.entry(key).or_insert(0);
		*entry += 1;
	}

	println!("Results: {:?}", scores);
}

